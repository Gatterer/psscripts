    ##Quelle:
    #https://gist.github.com/tobiashochguertel/618fdadf327cb1be5465edad8e6acc3b
    
    #Eingabedatei
    $InputFile = "F:\TG Scripts Git Repos\psscripts\WOL.txt"
    function Send-Packet([string]$MacAddress)
    {
    try
    {
    $Broadcast = [IPAddress] "172.25.255.255"
    ## Create UDP client instance
    $UdpClient = New-Object Net.Sockets.UdpClient
    ## Create IP endpoints for each port
    $IPEndPoint = New-Object Net.IPEndPoint $Broadcast, 9
    ## Construct physical address instance for the MAC address of the machine (string to byte array)
    $MAC = [Net.NetworkInformation.PhysicalAddress]::Parse($MacAddress.ToUpper())
    ## Construct the Magic Packet frame
    $Packet = [Byte[]](,0xFF*6)+($MAC.GetAddressBytes()*16)
    ## Broadcast UDP packets to the IP endpoint of the machine
    $UdpClient.Send($Packet, $Packet.Length, $IPEndPoint) | Out-Null
    $UdpClient.Close()
    }
    catch
    {
    $UdpClient.Dispose()
    $Error | Write-Error;
    }
    }
    #Fuer jede MAC in Zeile: Function 'Send-Paket + MAC'
    ForEach ($MacAddress in get-content $InputFile)
    {
    write-host "$MacAddress" 
    Send-Packet $MacAddress
    }