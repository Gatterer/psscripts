function Send-WOL
{
<# 
  .SYNOPSIS  
    Send a WOL packet to a broadcast address
  .PARAMETER mac
   The MAC address of the device that need to wake up
  .PARAMETER ip
   The IP address where the WOL packet will be sent to
  .EXAMPLE 
   Send-WOL -mac 00:11:32:21:2D:11 -ip 192.168.8.255 
#>

[CmdletBinding()]
param(
[Parameter(Mandatory=$True,Position=1)]
[string]$mac,
[string]$ip="255.255.255.255", 
[int]$port=9
)
$broadcast = [Net.IPAddress]::Parse($ip)
 
$mac=(($mac.replace(":","")).replace("-","")).replace(".","")
$target=0,2,4,6,8,10 | % {[convert]::ToByte($mac.substring($_,2),16)}
$packet = (,[byte]255 * 6) + ($target * 16)
 
$UDPclient = new-Object System.Net.Sockets.UdpClient
$UDPclient.Connect($broadcast,$port)
[void]$UDPclient.Send($packet, 102) 
}

Send-WOL -mac 90:b1:1c:97:84:e6 -ip 172.25.1.33
Send-WOL -mac 2c-27-d7-3f-8f-9f
Send-WOL -mac 24-be-05-25-4c-97
Send-WOL -mac 08-2e-5f-0b-18-a5
Send-WOL -mac 4c-cc-6a-c2-bd-11
Send-WOL -mac 08-2e-5f-04-fb-47
Send-WOL -mac 00-19-99-66-cb-e0
Send-WOL -mac 64-31-50-35-80-15
Send-WOL -mac 70-20-84-01-1f-87
Send-WOL -mac 2c-41-38-8f-a7-d9
Send-WOL -mac 64-31-50-31-9e-29
Send-WOL -mac d4-85-64-bd-26-37
Send-WOL -mac 64-31-50-1b-86-fb
Send-WOL -mac 3c-d9-2b-6d-2f-88
Send-WOL -mac d4-85-64-ba-45-e6
Send-WOL -mac d8-d3-85-81-07-04
Send-WOL -mac d4-85-64-c3-2d-5e
Send-WOL -mac 64-31-50-34-47-77
Send-WOL -mac 64-31-50-1c-cf-77
Send-WOL -mac d8-d3-85-7b-64-3d
Send-WOL -mac d8-d3-85-7f-31-4f
Send-WOL -mac 3c-d9-2b-6d-2f-8e
Send-WOL -mac 08-2e-5f-0b-57-71
Send-WOL -mac 2c-27-d7-3f-90-e8
Send-WOL -mac d8-d3-85-7f-30-ef
Send-WOL -mac 78-ac-c0-bc-ad-c3
Send-WOL -mac 2c-27-d7-3f-8f-a4
Send-WOL -mac 08-2e-5f-25-14-18
Send-WOL -mac d8-d3-85-7b-64-0c
Send-WOL -mac d8-d3-85-7b-68-da
Send-WOL -mac d8-d3-85-7f-31-a8
Send-WOL -mac 4c-cc-6a-c3-c0-a5
Send-WOL -mac 3c-d9-2b-73-5c-e5
Send-WOL -mac 70-20-84-01-1f-7c
Send-WOL -mac 3c-d9-2b-6d-2f-82